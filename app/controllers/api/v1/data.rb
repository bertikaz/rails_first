module API
	module V1
		class Data < Grape::API
			version 'v1'
			format :json

			resource :news do 
				desc "Return all news"
					get do 
						News.all
					end
			end

			resource :users do 
				desc "Return all users"
				get do 
					User.all
				end
			end

		end
	end
end