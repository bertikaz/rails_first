class NewsController < InheritedResources::Base
	def index
    @news = News.page(params[:page]).per(2)
  end

  private

    def news_params
      params.require(:news).permit(:title, :text)
    end
end

